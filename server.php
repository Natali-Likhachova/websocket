<?php
define('PORT', "8090");

require_once ("classes/chat.php");

$chat = new Chat();

//Создаёт сокет
$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);

//Устанавливает опции для сокета
socket_set_option($socket, SOL_SOCKET, SO_REUSEADDR, 1);

//Привязывает имя к сокету
socket_bind($socket,0,PORT);

//Прослушивает входящие соединения на сокете
socket_listen($socket);

$clientSocketArray = array($socket);

//Бесконечный цикл чтобы он постоянно выполнялся с консоли
while(true) {
    $newSocketArray = $clientSocketArray;
    $nullA = [];
    //Запускает системный вызов select() для заданных массивов сокетов с указанным тайм-аутом
    socket_select($newSocketArray, $nullA, $nullA, 0, 10);

    if(in_array($socket, $newSocketArray)){
        //Принимает соединение на сокете
        $newSocket = socket_accept($socket);
        $clientSocketArray[] = $newSocket;

        //Читает строку максимальную длину байт из сокета
        $header = socket_read($newSocket, 1024);
        $chat->sendHeaders($header,$newSocket,'localhost/chat',PORT);

        //--- узнаем ip клиента
        socket_getpeername($newSocket, $client_ip_address);
        $connectionACK = $chat->newConnectionACK($client_ip_address);
        $chat->send($connectionACK, $clientSocketArray);

        //--- удаляем сокет что уже обработали
        $newSocketArrayIndex = array_search($socket, $newSocketArray);
        unset($newSocketArray[$newSocketArrayIndex]);
    }

    foreach ($newSocketArray as $newSocketArrayResource) {
        //Получаем небольшими частями данные с сокета и отравляем их
        while (socket_recv($newSocketArrayResource, $socketData, 1024, 0) >= 1) {
            $socketMessage = $chat->unseal($socketData);
            $messageObj = json_decode($socketMessage);

            //--- готовое сообщение для отправки пользователю
            $chatMessage = $chat->createChatMessage($messageObj->chat_user, $messageObj->chat_message);

            $chat->send($chatMessage, $clientSocketArray);

            break 2;
        }

        //В случае выхода пользователя из чата, оповещаем всех
        $socketData = @socket_read($newSocketArrayResource,1024, PHP_NORMAL_READ);
        if($socketData === false) {
            //получение данных кто вышел и отправка всем остальным
            socket_getpeername($newSocketArrayResource, $client_ip_address);
            $connectionACK = $chat->newDisconnectionACK($client_ip_address);
            $chat->send($connectionACK,$clientSocketArray);

            //очистка массива от ненужных сокетов
            $newSocketArrayIndex = array_search($newSocketArrayResource, $clientSocketArray);
            unset($clientSocketArray[$newSocketArrayIndex]);
        }
    }
}

//Закрывает соединение сокета
socket_close($socket);