<?php


class Chat
{
    /*
     * Проверяем соединение
     */
    public function sendHeaders($headersText, $newSocket, $host, $post){
        $headers = array();
        $tmpLine = preg_split("/\r\n/",$headersText);

        foreach ($tmpLine as $line){
            $line = rtrim($line);
            if (preg_match('/\A(\S+): (.*)\z/',$line, $matches)) {
                $headers[$matches[1]] = $matches[2];
            }
        }

        $key = $headers['Sec-WebSocket-Key'];
        $sKey = base64_encode(pack('H*', sha1($key . '258EAFA5-E914-47DA-95CA-C5AB0DC85B11')));

        $strHeadr = "HTTP/1.1 101 Switching Protocols \r\n".
            "Upgrade: websocket\r\n".
            "Connection: Upgrade\r\n".
            "WebSocket-Origin: $host\r\n".
            "WebSocket-Location: ws://$host:$post/chat/server.php\r\n".
            "Sec-WebSocket-Accept: $sKey\r\n\r\n"
        ;

        socket_write($newSocket,$strHeadr, strlen($strHeadr));
    }


    /*
     * Формируем сообщение
     */
    public function newConnectionACK($client_ip_address){
        $message = "New client " . $client_ip_address . ' connected';
        $messageArray = [
            "message" => $message,
            "type" => "newConnectionACK"
        ];

        $ask = $this->seal(json_encode($messageArray));
        return $ask;
    }


    /*
     * Преобразует строковый формат в последовательность байт для отправки клиенту
     */
    public function seal($socketData){
        $b1 = 0x81;
        $length = strlen($socketData);
        $header = "";

        // 7 бит
        if($length <= 125){
            $header = pack('CC', $b1, $length);
        }// 7+16 бит
        elseif ($length > 125 && $length < 65536) {
            $header = pack('CCn', $b1, 126, $length);
        }// 7+64 бит
        elseif ($length > 65536) {
            $header = pack('CCNN', $b1, 127, $length);
        }

        return $header.$socketData;
    }


    /*
     * Отправка сообщения
     */
    public function send($message, $clientSocketArray){
        $messageLength = strlen($message);

        foreach ($clientSocketArray as $clientSocket){
            //Запись в сокет
            @socket_write($clientSocket, $message, $messageLength);
        }

        return true;
    }


    /*
     * Преобразует последовательность байт в JSON
     */
    public function unseal($socketData){
        $length = ord($socketData[1]) & 127;

        if($length == 126){
            $mask = substr($socketData,4,4);
            $data = substr($socketData,8);
        }
        elseif ($length == 127) {
            $mask = substr($socketData,10,4);
            $data = substr($socketData,14);
        }
        else {
            $mask = substr($socketData,2,4);
            $data = substr($socketData,6);
        }

        $socketStr = "";

        for ($i = 0; $i < strlen($data); ++$i){
            $socketStr .= $data[$i] ^ $mask[$i%4];
        }

        return $socketStr;
    }


    /*
     * Сообщение что увидит пользователь
     */
    public function createChatMessage($username, $messageStr){
        $mysqli = new mysqli("localhost", "root", "root", "chat");

        /* проверяем соединение */
        if (mysqli_connect_errno()) {
            printf("Connect failed: %s\n", mysqli_connect_error());
            exit();
        }
        $message_id = '1';
        $query = "SELECT message_id FROM carent_message ORDER BY message_id DESC LIMIT 1";

        if ($result = $mysqli->query($query)) {
            while ($row = $result->fetch_row()) {
                echo ' while ';
                $message_id = $row[0];
                $message_id = $message_id + 1;
                $messageStr = $messageStr . $message_id;
            }
            /* очищаем результирующий набор */
            $result->close();
        }
        echo '$messageStr ' . $messageStr;

        $mysqli->query("INSERT INTO `carent_message` VALUES ($message_id, 5580, 552, 1, '$messageStr', '', '2020-09-18 16:40:43', '1')");
        //$query = "SELECT `text` FROM `carent_message` WHERE `user_id`=552";

        $mysqli->close();


        $message = $username . "<div>" . $messageStr . "</div>";
        $messageArray = [
            'type' => 'chat-box',
            'message' => $message
        ];

        return $this->seal(json_encode($messageArray));
    }


    /*
     * Формирует сообщение о том что пользователь закрыл чат
     */
    public function newDisconnectionACK($client_ip_address){
        $message = "Client " . $client_ip_address . ' disconnected';
        $messageArray = [
            "message" => $message,
            "type" => "newConnectionACK"
        ];

        $ask = $this->seal(json_encode($messageArray));
        return $ask;
    }
}